var express = require('express');
var router = express.Router();
const Workorder = require('../../models/workorder');
const verify = require('./verifyToken')

router.get('/', function(req, res, next) {
    Workorder.findAll({
            where: {
                P_CHASSISNUMBER: req.query.chassis
            },
            include: [{ all: true, nested: true }]
        }).then(workorder => {
            res.send(workorder)
        })
        .catch(err => res.status(404).send(err))
});

module.exports = router;