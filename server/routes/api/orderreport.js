var express = require('express');
var router = express.Router();
const hanaClient = require("@sap/hana-client");
const connection = hanaClient.createConnection();
var Excel = require('exceljs');

const connectionParams = {
    host: "10.100.0.152",
    port: 33615,
    uid: "SYSTEM",
    pwd: "Hybr!S2015",
}

connection.connect(connectionParams, (err) => {
    if (err) {
        return console.error("Connection error", err);
    }

    router.get('/', function(req, res, next) {
        var workbook = new Excel.Workbook();

        workbook.creator = 'Mert CERTEL';
        // workbook.lastModifiedBy = 'Her';
        // workbook.created = new Date(1985, 8, 30);
        // workbook.modified = new Date();
        // workbook.lastPrinted = new Date(2016, 9, 27);
        // workbook.properties.date1904 = true;

        // workbook.views = [{
        //     x: 0,
        //     y: 0,
        //     width: 10000,
        //     height: 20000,
        //     firstSheet: 0,
        //     activeTab: 1,
        //     visibility: 'visible'
        // }];
        const sql = `select  o.CREATEDTS as OLUSTURMA_TARIHI,
            e.CODE as DURUM,
            cm.P_UID as ORGANIZASYON,
            u.P_UID as CARI_KOD,
            u.P_NAME as CARI_AD,
            o.P_CODE as SIPARIS_NO,
            p.P_CODE as MALZEME_NO,
            oe.P_QUANTITY as ADET,
            o.P_TOTALPRICE + o.P_TOTALTAX as TOPLAM_TUTAR,
            o.P_TOTALDISCOUNTS as ISKONTO_TUTARI,
            oe.P_TOTALPRICE *1.18 as TUTAR from ORDERS o
        left join  ORDERENTRIES oe on o.PK = oe.P_ORDER
        left join  CMSSITE cm on o.P_SITE = cm.PK
        left join USERS u on o.P_USER = u.PK
        left join PRODUCTS p on p.PK = oe.P_PRODUCT
        left join CRONJOBS cr on cr.PK = o.P_WORKFLOW
        left join ENUMERATIONVALUES e on o.P_STATUS = e.PK where o.P_STATUS  in(8796096266331,8796096036955,8796096725083)  and P_VERSIONID is  null and cr.P_STATUS not in (8796098920539,8796093251675)`;

        connection.exec(sql, (err, rows) => {
            if (err) {
                res.send(err)
            } else {
                var worksheet = workbook.addWorksheet('Bekleyen Siparisler');
                worksheet.columns = [
                    { header: 'OLUSTURMA_TARIHI', key: 'OLUSTURMA_TARIHI', type: 'date' },
                    { header: 'DURUM', key: 'DURUM' },
                    { header: 'ORGANIZASYON', key: 'ORGANIZASYON' },
                    { header: 'CARI_KOD', key: 'CARI_KOD' },
                    { header: 'CARI_AD', key: 'CARI_AD' },
                    { header: 'SIPARIS_NO', key: 'SIPARIS_NO' },
                    { header: 'MALZEME_NO', key: 'MALZEME_NO' },
                    { header: 'ADET', key: 'ADET' },
                    { header: 'TOPLAM_TUTAR(KDV DAHIL)', key: 'TOPLAM_TUTAR_KDV' },
                    { header: 'TOPLAM_TUTAR', key: 'TOPLAM_TUTAR' },
                    { header: 'ISKONTO_TUTARI', key: 'ISKONTO_TUTARI' },
                    { header: 'TUTAR', key: 'TUTAR' },

                ];
                rows.map((item, i) => {
                    worksheet.addRow({
                        id: i,
                        OLUSTURMA_TARIHI: new Date(item.OLUSTURMA_TARIHI),
                        DURUM: item.DURUM,
                        ORGANIZASYON: item.ORGANIZASYON,
                        CARI_KOD: item.CARI_KOD,
                        CARI_AD: item.CARI_AD,
                        SIPARIS_NO: item.SIPARIS_NO,
                        MALZEME_NO: item.MALZEME_NO,
                        ADET: +item.ADET,
                        TOPLAM_TUTAR: +item.TOPLAM_TUTAR / 1.18,
                        TOPLAM_TUTAR_KDV: +item.TOPLAM_TUTAR,
                        ISKONTO_TUTARI: +item.ISKONTO_TUTARI,
                        TUTAR: +item.TUTAR
                    });
                })
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
                workbook.xlsx.write(res)
                    .then(function(data) {
                        res.end()
                        console.log('Excel Download işlemi tamamlandı');
                    });
            }
        })
    });
});
connection.disconnect();
module.exports = router;