var express = require('express');
var router = express.Router();
const spauth = require('node-sp-auth');
const fetch = require('node-fetch');
router.post('/', function(req, res, next) {
    spauth
        .getAuth('http://intranet.uma.com.tr/sites/dev/', {
            username: req.body.username,
            password: req.body.password
        })
        .then(data => {
            let headers = data.headers;
            headers['Accept'] = 'application/json;odata=verbose';
            let requestOpts = data.options;
            requestOpts.json = true;
            requestOpts.headers = headers;
            //http://intranet.uma.com.tr/_api/web/GetUserById(5)/Groups
            fetch('http://intranet.uma.com.tr/_api/SP.UserProfiles.PeopleManager/GetMyProperties', requestOpts).then(response => {
                    return response.json();
                }).then(userProperties => {
                    res.send({ userProperties, requestOpts })
                })
                .catch(err => {
                    res.status(401).send('Giriş başarısız')
                })
        })
});

module.exports = router;