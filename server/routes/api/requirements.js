var express = require('express');
var router = express.Router();
const Brand = require('../../models/Brand');
const BrandCategory = require('../../models/BrandCategory');
const CloseReason = require('../../models/CloseReason');
const Priority = require('../../models/Priority');
const ReasonCategory = require('../../models/ReasonCategory');
const ReasonSubCategory = require('../../models/ReasonSubCategory');
const RootReason = require('../../models/RootReason');
const SatisfiedState = require('../../models/SatisfiedState');
const Source = require('../../models/Source');
const State = require('../../models/State');
const User = require('../../models/User');
const Type = require('../../models/Type');
const Sparepartstype = require('../../models/Sparepartstype');
const Templates = require('../../models/Templates')
const fetch = require('node-fetch');

// router.get('/spareparts', function (req, res, next) {
//     Spareparts.findAll().then(Spareparts => {
//         res.send(Spareparts)
//     }).catch(err => {
//         res.status(400).send(err)
//     })
// });

// router.get('/spareparts', function(req, res, next) {
//     fetch(`http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/CRMKategoriSet?$filter=Altkgt eq '${req.query.subCategory}'&$format=json`)
//         .then(res1 => res1.json())
//         .then(json => {
//             let result = json.d.results.map(item => {
//                 delete item.__metadata
//                 delete item.Mandt
//                 delete item.A1
//                 delete item.Mrktx
//                 delete item.Anakgt
//                 delete item.Rsira
//                 delete item.Padet
//                 return item
//             })
//             res.send(result)
//         })
//         .catch(err => res.send(err))
// });
router.get('/model', function(req, res, next) {
    fetch(`http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/TblExportSet?$filter=( Matnr eq 'M' or Matnr eq 'T' ) and Werks eq '2100' and Zzmarka eq '${req.query.brand}'&$format=json`)
        .then(res1 => res1.json())
        .then(json => res.send(json))
        .catch(err => res.send(err))
});
router.get('/brand', function(req, res, next) {
    Brand.findAll().then(Brand => {
            res.send(Brand)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/getbrandcategories', function(req, res, next) {
    BrandCategory.findAll({ where: { BRAND_PK: req.query.brand } }).then(BrandCategory => {
        res.send(BrandCategory)
    }).catch(err => {
        res.status(400).send(err)
    })
});
router.get('/getsubcategories', function(req, res, next) {
    fetch(`http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/CRMKategoriSet?$filter=Anakg eq '${req.query.category}' and Mrkid eq '${req.query.brand}'&$format=json`)
        .then(res1 => res1.json())
        .then(json => {
            const uniqueArr = [...new Set(json.d.results.map(data => data.Altkgt))]
            res.send(uniqueArr)
        })
        .catch(err => res.send(err))
});
router.get('/closereason', function(req, res, next) {
    CloseReason.findAll().then(CloseReason => {
            res.send(CloseReason)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/priority', function(req, res, next) {
    Priority.findAll().then(val => {
            res.send(val)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/reasoncategory', function(req, res, next) {
    ReasonCategory.findAll().then(ReasonCategory => {
            res.send(ReasonCategory)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/reasonsubcategory', function(req, res, next) {
    ReasonSubCategory.findAll().then(ReasonSubCategory => {
            res.send(ReasonSubCategory)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/rootreason', function(req, res, next) {
    RootReason.findAll().then(RootReason => {
            res.send(RootReason)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/satisfiedstate', function(req, res, next) {
    SatisfiedState.findAll().then(SatisfiedState => {
            res.send(SatisfiedState)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/source', function(req, res, next) {
    Source.findAll().then(Source => {
            res.send(Source)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});



router.get('/state', function(req, res, next) {
    State.findAll().then(State => {
            res.send(State)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/type', function(req, res, next) {
    Type.findAll().then(Type => {
            res.send(Type)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/mailtemplates', function(req, res, next) {
    Templates.findAll().then(Templates => {
            res.send(Templates)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/sparepartstype', function(req, res, next) {
    Sparepartstype.findAll().then(Sparepartstype => {
            res.send(Sparepartstype)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.get('/responsible', function(req, res, next) {
    User.findAll().then(UserData => {

        res.send(UserData.map(item => {
            return {
                PK: item.dataValues.PK,
                NAME: item.dataValues.DISPLAY_NAME,
            }
        }))

    }).catch(err => {
        res.status(400).send(err)
    })
});




module.exports = router;