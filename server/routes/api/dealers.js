var express = require('express');
var router = express.Router();
const Dealers = require('../../models/Dealers');
const Sequelize = require('Sequelize');
const Op = Sequelize.Op;
router.get('/', function(req, res, next) {
    Dealers.findAll({
            where: {
                [Op.or]: [{
                    M_ADI: {
                        [Op.like]: `%${req.query.M_ADI}%`
                    }
                }, {
                    M_KODU: {
                        [Op.like]: `%${req.query.M_ADI}%`
                    }
                }]

            },
        }).then(dealers => {
            res.send(dealers)
        })
        .catch(err => res.status(404).send(err))
});

module.exports = router;