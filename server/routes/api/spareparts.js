var express = require('express');
var router = express.Router();
const Spareparts = require('../../models/Spareparts');
const fetch = require('node-fetch');
const verify = require('./verifyToken')
    // router.get('/spareparts', function (req, res, next) {
    //     Spareparts.findAll().then(Spareparts => {
    //         res.send(Spareparts)
    //     }).catch(err => {
    //         res.status(400).send(err)
    //     })
    // });

router.get('/', verify, function(req, res, next) {
    fetch(`http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/CRMKategoriSet?$filter=Altkgt eq '${req.query.subCategory}'&$format=json`)
        .then(res1 => res1.json())
        .then(json => {
            let result = json.d.results.map(item => {
                delete item.__metadata
                delete item.Mandt
                delete item.A1
                delete item.Mrktx
                delete item.Anakgt
                delete item.Rsira
                delete item.Padet
                return item
            })
            res.send(result)
        })
        .catch(err => res.send(err))
});

router.post('/add', verify, function(req, res, next) {
    let {
        Ailei,
        Altkg,
        Altkgt,
        Anakg,
        Labstgs,
        Labstks,
        Maktx,
        Matnr,
        Mrkid,
        TICKET_PK,
        PerakendeFiyat
    } = req.body
    Spareparts.create({
            CREATEDBY: req.user.PK,
            Ailei,
            Altkg,
            Altkgt,
            Anakg,
            Labstgs,
            Labstks,
            Maktx,
            Matnr,
            Mrkid,
            TICKET_PK,
            PerakendeFiyat
        }).then(Spareparts => {
            res.send(Spareparts)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.delete('/delete/', verify, function(req, res, next) {
    Spareparts.destroy({
        where: {
            PK: req.query.PK
        }
    }).then((val) => {
        res.send(200)
    }).catch(err => {
        res.send(err)
    })
});


module.exports = router;