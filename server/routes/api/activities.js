var express = require('express');
var router = express.Router();
const Activity = require('../../models/Activity');
const fetch = require('node-fetch');
const verify = require('./verifyToken')
router.get('/latest', verify, function(req, res, next) {
    Activity.findOne({
            where: {
                TICKET_PK: req.query.PK
            },
            order: [
                ['CREATEDTS', 'DESC'],
            ],
        }).then(Activity => {
            res.send(Activity)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.post('/add', verify, function(req, res, next) {
    let {
        TICKET_PK,
        TYPE,
        TEXT,
        TEMP_PK
    } = req.body
    Activity.create({
            CREATEDBY: req.user.PK,
            TICKET_PK,
            TYPE,
            TEXT,
            TEMP_PK

        }).then(Activity => {
            res.send(Activity)
        }).catch(err => {
            res.status(400).send(err)
        })
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.delete('/delete/', verify, function(req, res, next) {
    Activity.destroy({
        where: {
            PK: req.query.PK
        }
    }).then(() => {
        res.sendStatus(200)
    }).catch(err => {
        res.sendStatus(400)
    })
});

router.put('/update/', verify, function(req, res, next) {
    let {
        PK,
        TEXT
    } = req.body
    Activity.update({
        TEXT
    }, {
        where: {
            PK: PK
        }
    }).then(Activity => {
        res.send(Activity)
    }).catch(err => {
        res.status(400).send(err)
    })
});


module.exports = router;