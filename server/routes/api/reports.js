var express = require('express');
var router = express.Router();
const Report = require('../../models/Report');
const verify = require('./verifyToken')
const Sequelize = require('sequelize');
const Ope = Sequelize.Op;


router.get('/', async function(req, res, next) {
    let betweenDates = req.query.dates.split(",");
    try {
        let data = await Report.findAll({
            where: {
                [Ope.or]: [{
                    SHORT_CREATEDTS: {
                        [Ope.between]: [betweenDates[0], betweenDates[1]]
                    }
                }, {
                    SHORT_CREATEDTS: {
                        [Ope.between]: [betweenDates[1], betweenDates[0]]
                    }
                }],

            },
            order: [
                ['CREATEDTS', 'DESC'],
                ['STATE', 'ASC']
            ],

        })
        res.send(data)
    } catch (err) {
        res.sendStatus(400).send(err)
    }
});

module.exports = router;