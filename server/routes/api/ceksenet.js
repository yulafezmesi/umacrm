var express = require('express');
var router = express.Router();
const fetch = require('node-fetch');
/* GET users listing. */
router.get('/', function(req, res, next) {
    fetch("http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/CekSenetSet?$format=json")
        .then(res1 => res1.json())
        .then(json => {
            json.d.results.forEach(function(item, index) {
                let dateVade = new Date(parseInt(item.VadeTarihi.slice(6, -2)));
                let dateNewVade = dateVade.toLocaleDateString('tr-TR', {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                })
                let dateKayit = new Date(parseInt(item.KayitTarihi.slice(6, -2)));
                let dateNewKayit = dateKayit.toLocaleDateString('tr-TR', {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                })
                json.d.results[index].VadeTarihi = dateNewVade;
                json.d.results[index].KayitTarihi = dateNewKayit;
            });

            res.send(json.d.results)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
});


router.get('/export', function(req, res, next) {
    fetch("http://umnnamsv0224.ugurgrp.local:8000/sap/opu/odata/sap/ZBS003_SRV/CekSenetSet?$format=xlsx")
        .then(xlsx => {
            res.send(xlsx)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
});
module.exports = router;