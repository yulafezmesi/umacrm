var express = require('express');
var router = express.Router();
const Ticket = require('../../models/Ticket');
const shortid = require('shortid');
const verify = require('./verifyToken')
const user = require('../../models/User')
const ReasonCategory = require('../../models/ReasonCategory')
const Priority = require('../../models/Priority')
const Source = require('../../models/Source')
const State = require('../../models/State')
const Customer = require('../../models/Customers')
const ResponsibleUser = require('../../models/User')
const Sequelize = require('sequelize');
const Ope = Sequelize.Op;
router.get('/count', verify, async function(req, res, next) {
    Ticket.count({
        col: 'PK'
    }).then(count => {
        res.send({ count })
    }).catch(err => {
        console.log(err)
        res.status(400).send(err)
    })
});


router.get('/', verify, async function(req, res, next) {
    let filteredArr = [];
    if (req.query.query) {
        let stringArr = JSON.stringify(req.query.query)
        let parsedArr = JSON.parse(stringArr)
        parsedArr.map(item => {
            let bItem = JSON.parse(item)
            let filteredObj = {};
            if (bItem.op == "equal") {
                filteredObj[bItem.key] = bItem.value;
            } else if (bItem.op == "contain") {
                filteredObj[bItem.key] = {
                    [Ope.like]: `%${bItem.value}%`
                };
            } else if (bItem.op == "between") {
                let begin = bItem.value.split(',')[0];
                let end = bItem.value.split(',')[1];
                filteredObj[bItem.key] = {
                    [Ope.between]: [begin, end]
                }
            }
            filteredArr.push(filteredObj);
        })

    }
    try {
        let data = await Ticket.findAll({
            include: [
                { model: Priority, as: "Priority" },
                { model: Source, as: "Source" },
                { model: State, as: "State" },
                { model: Customer, as: "Customer" },
                { model: ResponsibleUser, as: "ResponsibleUser" },
            ],
            where: {
                [Ope.and]: filteredArr,
                // [Ope.or]: [{
                //     CREATEDTS: {
                //         [Ope.between]: [betweenDates[0], betweenDates[1]]
                //     }
                // }, {
                //     CREATEDTS: {
                //         [Ope.between]: [betweenDates[1], betweenDates[0]]
                //     }
                // }],

            },
            limit: +req.query.limit, // Dynamic from query string.
            offset: +req.query.offset,
            order: [
                ['CREATEDTS', 'DESC'],
                ['STATE_PK', 'ASC']
            ],

        })
        res.send(data)
    } catch (err) {
        res.sendStatus(400).send(err)
    }
    // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/my', verify, async function(req, res, next) {
    try {
        let data = await Ticket.findAll({
            where: { RESPONSIBLE: req.user.PK },
            include: [
                { model: Priority, as: "Priority" },
                { model: Source, as: "Source" },
                { model: State, as: "State" },
                { model: Customer, as: "Customer" },
                { model: ResponsibleUser, as: "ResponsibleUser" },
            ],
            limit: +req.query.limit, // Dynamic from query string.
            offset: +req.query.offset,
            order: [
                ['CREATEDTS', 'DESC'],
                ['STATE_PK', 'ASC']
            ],

        })
        res.send(data)
    } catch (err) {

    }
});
router.get('/:pk', function(req, res, next) {
    Ticket.findOne({ where: { PK: req.params.pk }, include: [{ all: true, nested: true }] }).then(Ticket => {
        res.send(Ticket)
    }).catch(err => res.status(404).send(err))
});

const getUpdateDate = async(PK) => {
    try {
        let data = await Ticket.findOne({ where: { PK } });
        return data.SOLUTION_DATE

    } catch (e) {
        console.log(e)
    }

}


router.put('/update', verify, async function(req, res, next) {

    let {
        PK,
        SOURCE_PK,
        RESPONSIBLE,
        TYPE_PK,
        PRIORITY_PK,
        ROOTREASON_PK,
        STATE_PK,
        REASONCATEGORY_PK,
        REASONSUBCATEGORY_PK,
        CLOSEREASON_PK,
        CHASSIS_NUMBER,
        BRAND_PK,
        CATEGORY_PK,
        MODEL,
        SATISFIEDSTATE_PK,
        SPAREPARTSTYPE_PK,
        DEALER_CODE,
        ORDER_NO,
        WORKORDER_NO
    } = req.body

    let updatedObj = {
        PK,
        SOURCE_PK,
        RESPONSIBLE,
        TYPE_PK,
        PRIORITY_PK,
        MODIFIEDBY: req.user.PK,
        ROOTREASON_PK,
        STATE_PK,
        REASONCATEGORY_PK,
        REASONSUBCATEGORY_PK,
        CLOSEREASON_PK,
        CHASSIS_NUMBER,
        BRAND_PK,
        CATEGORY_PK,
        MODEL,
        SATISFIEDSTATE_PK,
        SPAREPARTSTYPE_PK,
        DEALER_CODE,
        ORDER_NO,
        WORKORDER_NO,
    }

    let checkSolutionDate = await getUpdateDate(PK)
    if (STATE_PK === 3 && !checkSolutionDate) {
        updatedObj.SOLUTION_DATE = Sequelize.literal('CURRENT_TIMESTAMP');

    }
    Ticket.update(updatedObj, {
        where: {
            PK: PK
        }
    }).then(ticket => {
        res.send(ticket)
    }).catch(err => {
        res.status(400).send(err)
    })
});
router.post('/add', verify, function(req, res, next) {
    let { SOURCE_PK, TYPE_PK, CUSTOMER_PK, PRIORITY_PK, ACTIVITY, RESPONSIBLE } = req.body
    Ticket.create({
        PK: shortid.generate(),
        CREATEDBY: req.user.PK,
        RESPONSIBLE: RESPONSIBLE,
        TYPE_PK: TYPE_PK,
        SOURCE_PK: SOURCE_PK,
        CUSTOMER_PK: CUSTOMER_PK,
        PRIORITY_PK: PRIORITY_PK,
        SUBJECT: ACTIVITY,
        STATE_PK: 1,
        Activities: [{ CREATEDBY: req.user.PK, TYPE: "Text", TEXT: ACTIVITY }]
    }, { include: ["Activities"] }).then(Ticket => {
        res.send(Ticket)
    }).catch(err => {
        console.log(err)
        res.status(400).send(err)
    })
});



module.exports = router;