var express = require('express');
var router = express.Router();
const User = require('../../models/User');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const verify = require('./verifyToken')

router.post('/register', verify, async function(req, res, next) {
    let {
        DISPLAY_NAME,
        EMAIL,
        USERNAME,
        PASSWORD,
        ROLE_PK
    } = req.body

    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(PASSWORD, salt)

    User.create({
        DISPLAY_NAME,
        EMAIL,
        USERNAME,
        PASSWORD: hashPassword,
        ROLE_PK
    }).then(User => {
        res.send(User)
    }).catch(err => {
        res.status(400).send(err)
    })
});

router.get('/getCurrentUser', verify, async(req, res, next) => {
    const token = req.header('auth-token');
    const verified = jwt.verify(token, process.env.TOKEN_SECRET)
    console.log()
    const user = await User.findOne({ where: { PK: verified.PK }, include: ["Role"] })
    delete user.dataValues.PASSWORD
    res.send(user)
})

router.post('/login', async function(req, res, next) {
    let {
        USERNAME,
        PASSWORD,
    } = req.body

    const user = await User.findOne({ where: { USERNAME } })
    if (!user) res.status(400).send({ message: "Kullanıcı bulunamadı" })
    const validPass = await bcrypt.compare(PASSWORD, user.dataValues.PASSWORD)
    if (!validPass) res.status(401).send({ message: "Kullanıcı adı ve ya parola hatalı" })
    const token = jwt.sign({ PK: user.PK, USERNAME }, process.env.TOKEN_SECRET)
    res.header('auth-token', token).send('Giriş başarılı...')

});



module.exports = router;









// const spauth = require('node-sp-auth');
// const fetch = require('node-fetch');

// module.exports = function(req, res, next) {
//     const token = req.headers;
//     token['Accept'] = 'application/json;odata=verbose';
//     token.json = true;
//     console.log(token)
//         // if (!token) return res.status(401).send('Giriş Reddedildi')
//     fetch('http://intranet.uma.com.tr/_api/SP.UserProfiles.PeopleManager/GetMyProperties', token).then(response => {
//             return response.json();
//         }).then(userProperties => {
//             next()
//         })
//         .catch(err => {
//             res.status(401).send('Giriş başarısız')
//         })

// }