var express = require('express');
var router = express.Router();
const Customers = require('../../models/Customers');
const shortid = require('shortid');
const Sequelize = require('Sequelize');
const Op = Sequelize.Op;
const verify = require('./verifyToken')




router.get('/count', verify, async function(req, res, next) {
    Customers.count({
        col: 'PK'
    }).then(count => {
        res.send({ count })
    }).catch(err => {
        console.log(err)
        res.status(400).send(err)
    })
});

/* GET users listing. */

router.get('/', verify, function(req, res, next) {
    Customers.findAll({
        limit: +req.query.limit, // Dynamic from query string.
        offset: +req.query.offset,
        order: [
            ['CREATEDTS', 'DESC'],
        ],

    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log(err)
        res.status(400).send(err)
    })
});
router.get('/:id', verify, function(req, res, next) {
    Customers.findOne({
            where: {
                PK: req.params.id
            },
            include: [{ all: true, nested: true }]
        }).then(user => {
            res.send(user)
        })
        .catch(err => res.status(404).send(err))
        // User.findAll()
        //     .then(users => res.send(users))
        //     .catch(err => console.log(err))
        // res.send('respond asdasd a resdd  ourcfe s');
});
router.get('/ara/:name', verify, function(req, res, next) {
    Customers.findAll({
            where: {
                P_NAME: {
                    [Op.like]: `%${req.params.name}%`
                }
            },
            limit: 10
        }).then(user => {
            res.status(200).send(user)
        })
        .catch(err => res.status(404).send(err))
        // User.findAll()
        //     .then(users => res.send(users))
        //     .catch(err => console.log(err))
        // res.send('respond asdasd a resdd  ourcfe s');
});

router.post('/add', verify, function(req, res, next) {
    let { P_NAME, P_TELNO, P_ADRESS, P_EMAIL, P_IL, P_ILCE, P_SABITTEL } = req.body
    let errors = [];
    // Validate Fields
    Customers.create({
        PK: shortid.generate(),
        P_NAME,
        P_TELNO,
        P_SABITTEL,
        P_ADRESS,
        P_EMAIL,
        P_IL,
        P_ILCE,
        CREATEDBY: req.user.PK

    }).then(data => {
        res.status(201).send(data)
    }).catch(err => {
        res.status(400).send({ message: 'err.errors' })
    })


});

router.put('/update', verify, function(req, res, next) {
    let { P_NAME, P_TELNO, P_EMAIL, P_IL, P_ILCE, P_SABITTEL, PK } = req.body
    Customers.update({
        P_NAME,
        P_TELNO,
        P_SABITTEL,
        P_EMAIL,
        P_IL,
        P_ILCE,
        MODIFIEDBY: req.user.PK

    }, {
        where: {
            PK
        }
    }).then(data => {
        res.send(data)
    }).catch(err => {
        console.log('Müşteri Güncelleme', err)
        res.send(err)
    })

});

module.exports = router;