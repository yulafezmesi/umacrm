const Sequelize = require('Sequelize');
const db = require('../config/database')
const Role = require('./Role')
const User = db.define('USERS', {
    PK: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,

    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    DISPLAY_NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    EMAIL: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    USERNAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    PASSWORD: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ROLE_PK: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {

    timestamps: true,
    tableName: 'USERS',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS',
})

User.belongsTo(Role, { as: "Role", foreignKey: 'ROLE_PK' })

module.exports = User;