const Sequelize = require('Sequelize');
const db = require('../config/database')
const Brand = require('./Brand');
const BrandCategory = require('./BrandCategory');
const CloseReason = require('./CloseReason');
const Priority = require('./Priority');
const ReasonCategory = require('./ReasonCategory');
const ReasonSubCategory = require('./ReasonSubCategory');
const RootReason = require('./RootReason');
const SatisfiedState = require('./SatisfiedState');
const Source = require('./Source');
const Spareparts = require('./Spareparts');
const State = require('./State');
const Type = require('./Type');
const Activity = require('../models/Activity');
const Customer = require('../models/Customers');
const User = require('../models/User');
const Invoice = require('../models/Invoice');
const Workorder = require('../models/Workorder');

const Ticket = db.define('TICKETS', {
    PK: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    CREATEDBY: {
        type: Sequelize.INTEGER
    },
    MODIFIEDBY: {
        type: Sequelize.INTEGER
    },
    RESPONSIBLE: {
        type: Sequelize.INTEGER
    },
    SUBJECT: {
        type: Sequelize.STRING
    },
    IS_DELETED: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    TYPE_PK: {
        type: Sequelize.INTEGER
    },
    PRIORITY_PK: {
        type: Sequelize.INTEGER
    },
    SOURCE_PK: {
        type: Sequelize.INTEGER
    },
    ROOTREASON_PK: {
        type: Sequelize.INTEGER
    },
    STATE_PK: {
        type: Sequelize.INTEGER
    },
    CUSTOMER_PK: {
        type: Sequelize.STRING
    },
    REASONCATEGORY_PK: {
        type: Sequelize.INTEGER
    },
    REASONSUBCATEGORY_PK: {
        type: Sequelize.INTEGER
    },
    SPAREPARTSTYPE_PK: {
        type: Sequelize.INTEGER
    },
    BRAND_PK: {
        type: Sequelize.INTEGER
    },
    DEALER_CODE: {
        type: Sequelize.STRING
    },
    CATEGORY_PK: {
        type: Sequelize.INTEGER
    },
    MODEL: {
        type: Sequelize.STRING
    },
    DEALER_CODE: {
        type: Sequelize.INTEGER
    },
    ORDER_NO: {
        type: Sequelize.STRING
    },
    WORKORDER_NO: {
        type: Sequelize.STRING
    },
    CHASSIS_NUMBER: {
        type: Sequelize.STRING,
        references: {
            model: 'WORKORDER',
            key: 'P_CHASSISNUMBER'
        }

    },
    SATISFIEDSTATE_PK: {
        type: Sequelize.INTEGER
    },
    SOLUTION_DATE: {
        type: Sequelize.DATE
    },

}, {
    timestamps: true,
    tableName: 'TICKETS',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS'
})
Ticket.belongsTo(Type, { as: "Type", foreignKey: 'TYPE_PK' })
Ticket.belongsTo(CloseReason, { as: "CloseReason", foreignKey: 'CLOSEREASON_PK' })
Ticket.belongsTo(Priority, { as: "Priority", foreignKey: 'PRIORITY_PK' })
Ticket.belongsTo(ReasonCategory, { as: "ReasonCategory", foreignKey: 'REASONCATEGORY_PK' })
Ticket.belongsTo(ReasonSubCategory, { as: "ReasonSubCategory", foreignKey: 'REASONSUBCATEGORY_PK' })
Ticket.belongsTo(RootReason, { as: "RootReason", foreignKey: 'ROOTREASON_PK' })
Ticket.belongsTo(SatisfiedState, { as: "SatisfiedState", foreignKey: 'SATISFIEDSTATE_PK' })
Ticket.belongsTo(Source, { as: "Source", foreignKey: 'SOURCE_PK' })
Ticket.belongsTo(State, { as: "State", foreignKey: 'STATE_PK' })
Ticket.belongsTo(Customer, { as: "Customer", foreignKey: 'CUSTOMER_PK' })
Ticket.belongsTo(Brand, { as: "Brand", foreignKey: 'BRAND_PK' })
Ticket.belongsTo(User, { as: "CreatedUser", foreignKey: 'CREATEDBY' })
Ticket.belongsTo(User, { as: "ModifiedUser", foreignKey: 'MODIFIEDBY' })
Ticket.belongsTo(User, { as: "ResponsibleUser", foreignKey: 'RESPONSIBLE' })
    // Activity.belongsTo(Ticket);
Ticket.hasMany(Activity, { as: "Activities", foreignKey: "TICKET_PK" })
Ticket.hasMany(Spareparts, { as: "Spareparts", foreignKey: "TICKET_PK" })
Customer.hasMany(Ticket, { as: "Tickets", foreignKey: "Customer_PK" })
    // Ticket.belongsToMany(Workorder, { as: "Workorders", otherKey: "P_CHASSISNUMBER", through: 'WORKORDER' })
    // Ticket.hasMany(Invoice, { as: "Invoice", foreignKey: "CHASSIS_NUMBER", targetKey: 'P_CHASSISNUMBER' })

module.exports = Ticket;