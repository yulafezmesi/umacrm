const Sequelize = require('Sequelize');
const db = require('../config/database')

const Source = db.define('TICKETSOURCES', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,

    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    CREATEDBY: {
        type: Sequelize.INTEGER
    },
    MODIFIEDBY: {
        type: Sequelize.INTEGER
    },
    NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },


}, {
    timestamps: true,
    tableName: 'TICKETSOURCES',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS',
})

module.exports = Source;