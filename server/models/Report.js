const Sequelize = require('Sequelize');
const db = require('../config/database')

const Ticket = db.define('CRM_REPORT', {
    PK: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    TYPE: {
        type: Sequelize.STRING
    },
    SUBJECT: {
        type: Sequelize.STRING
    },
    STATE: {
        type: Sequelize.STRING
    },
    RESPONSIBLE: {
        type: Sequelize.STRING
    },
    SOURCE: {
        type: Sequelize.STRING
    },
    MOUNT: {
        type: Sequelize.STRING
    },
    YEAR: {
        type: Sequelize.STRING,
    },
    CUSTOMER_PK: {
        type: Sequelize.STRING
    },
    P_NAME: {
        type: Sequelize.STRING
    },
    P_TELNO: {
        type: Sequelize.STRING
    },
    P_IL: {
        type: Sequelize.STRING
    },
    P_ILCE: {
        type: Sequelize.STRING
    },
    PRIORTY: {
        type: Sequelize.INTEGER
    },
    SOLUTION_DATE: {
        type: Sequelize.DATE
    },
    SOLUTION_DATE_DIFF: {
        type: Sequelize.INTEGER
    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    SHORT_CREATEDTS: {
        type: Sequelize.DATE
    },
    LAST_COMMEND: {
        type: Sequelize.STRING
    },
    REASON_CATEGORY: {
        type: Sequelize.STRING
    },
    REASON_SUB_CATEGORY: {
        type: Sequelize.STRING
    },
    ROOT_REASON: {
        type: Sequelize.STRING
    },
    CLOSE_REASON: {
        type: Sequelize.STRING
    },
    SPAREPARTS: {
        type: Sequelize.STRING
    },
    DEALER_CODE: {
        type: Sequelize.STRING
    },
    DEALER_NAME: {
        type: Sequelize.STRING
    },

    SATISFIEDSTATE: {
        type: Sequelize.STRING
    },
    BRAND: {
        type: Sequelize.STRING
    },

    MODEL: {
        type: Sequelize.STRING
    },

}, {
    timestamps: false,
    tableName: 'CRM_REPORT',
})

module.exports = Ticket;