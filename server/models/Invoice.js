const Sequelize = require('Sequelize');
const db = require('../config/sapmasterdb.js')

const Invoice = db.define('SAPINVOICESERIAL', {
    HJMPTS: { type: Sequelize.BIGINT },
    CREATEDTS: { type: Sequelize.DATE },
    MODIFIEDTS: { type: Sequelize.DATE },
    TYPEPKSTRING: { type: Sequelize.BIGINT },
    PK: { type: Sequelize.BIGINT, primaryKey: true },
    P_MATERIALCODE: { type: Sequelize.STRING },
    P_MATERIALTYPE: { type: Sequelize.STRING },
    P_MATERIALDESC: { type: Sequelize.STRING },
    P_CHASSISNUMBER: { type: Sequelize.STRING },
    P_ENGINENUMBER: { type: Sequelize.STRING },
    P_MODALYEAR: { type: Sequelize.STRING },
    P_BRANDCODE: { type: Sequelize.STRING },
    P_BRANDDESC: { type: Sequelize.STRING },
    P_PYPMODAL: { type: Sequelize.STRING },
    P_ASSETGROUP: { type: Sequelize.STRING },
    P_CONTRACT: { type: Sequelize.BIGINT },
    P_INVOICENUMBER: { type: Sequelize.STRING },
    P_PAYER: { type: Sequelize.STRING },
    P_PAYERNAME: { type: Sequelize.STRING },
    P_INVOICEDATE: { type: Sequelize.DATE },
    ACLTS: { type: Sequelize.BIGINT }


}, {
    timestamps: false,
    tableName: 'SAPINVOICESERIAL',
})


module.exports = Invoice;