const Sequelize = require('Sequelize');
const db = require('../config/sapmasterdb')

const Dealers = db.define('SAP_MUSTERILER', {
    M_KODU: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    M_ADI: {
        type: Sequelize.STRING,
    },
    M_IL: {
        type: Sequelize.STRING,
    },
    M_ILCE: {
        type: Sequelize.STRING,
    },
    M_TELEFON: {
        type: Sequelize.STRING,
    },
    M_MAIL: {
        type: Sequelize.STRING,
    },


}, {
    timestamps: false,
    tableName: 'SAP_MUSTERILER',
})

module.exports = Dealers;