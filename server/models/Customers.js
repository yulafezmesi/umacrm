const Sequelize = require('Sequelize');
const db = require('../config/database')
const uuid = require('uuid/v4');
const ShortUniqueId = require('short-unique-id');
var uid = new ShortUniqueId();


const Customer = db.define('CUSTOMERS',

    {
        HJMPTS: {
            type: Sequelize.BIGINT
        },
        CREATEDTS: {
            type: Sequelize.DATE
        },
        MODIFIEDTS: {
            type: Sequelize.DATE
        },
        CREATEDBY: {
            type: Sequelize.INTEGER
        },
        MODIFIEDBY: {
            type: Sequelize.INTEGER
        },
        TYPEPKSTRING: {
            type: Sequelize.BIGINT
        },
        PK: {
            primaryKey: true,
            type: Sequelize.STRING,

        },
        P_NAME: {
            type: Sequelize.STRING,
            allowNull: false,
            trim: true,
            validate: {
                notNull: { msg: "İsim Gereklidir" },
            },
        },
        P_TELNO: {
            type: Sequelize.STRING(255)
        },
        P_SABITTEL: {
            type: Sequelize.STRING
        },
        P_ACTIVE: {
            type: Sequelize.BOOLEAN(),
            defaultValue: true
        },
        P_EMAIL: {
            type: Sequelize.STRING(255)
        },
        P_ADRES: {
            type: Sequelize.STRING(255)
        },
        P_IL: {
            type: Sequelize.STRING(50)
        },
        P_ILCE: {
            type: Sequelize.STRING(50)
        },
        P_KARALISTE: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        P_CINSIYET: {
            type: Sequelize.STRING(10)
        }

    }, {
        timestamps: true,
        tableName: 'CUSTOMERS',
        createdAt: 'CREATEDTS',
        updatedAt: 'MODIFIEDTS',
    })

// Customer.hasMany(models.Ticket, { CUSTOMER_PK: 'ID', as: 'Tickets' });

module.exports = Customer;