const Sequelize = require('Sequelize');
const db = require('../config/sapmasterdb.js')

const Workorder = db.define('WORKORDERLINE', {
    HJMPTS: { type: Sequelize.BIGINT },
    CREATEDTS: { type: Sequelize.DATE },
    MODIFIEDTS: { type: Sequelize.DATE },
    TYPEPKSTRING: { type: Sequelize.BIGINT },
    PK: { type: Sequelize.BIGINT, primaryKey: true },
    SEALED: { type: Sequelize.INTEGER },
    P_LINETYPE: { type: Sequelize.BIGINT },
    P_MATERIALCODE: { type: Sequelize.STRING },
    P_QUANTITY: { type: Sequelize.BIGINT },
    P_MEASUREUNIT: { type: Sequelize.STRING },
    P_UNITPRICE: { type: Sequelize.INTEGER },
    P_KDV: { type: Sequelize.INTEGER },
    P_DISCOUNTPRICE: { type: Sequelize.INTEGER },
    P_FAULTREASON: { type: Sequelize.STRING },
    P_WORKORDERPAYMENTTYPE: { type: Sequelize.BIGINT },
    P_SAPORDERNO: { type: Sequelize.STRING },
    P_SAPORDERSTATUS: { type: Sequelize.STRING },
    P_NETAMOUNT: { type: Sequelize.STRING },
    P_SAPSENTNO: { type: Sequelize.STRING },
    P_RETURNORDER: { type: Sequelize.STRING },
    P_RETURNITEM: { type: Sequelize.STRING },
    P_RETURNSTATUS: { type: Sequelize.STRING },
    P_SAPITEMNO: { type: Sequelize.STRING },
    P_ISSENT: { type: Sequelize.INTEGER },
    P_SAPRETURNORDERSTATUS: { type: Sequelize.INTEGER },
    P_ISOLD: { type: Sequelize.INTEGER },
    P_WORKORDER: { type: Sequelize.BIGINT },
    ACLTS: { type: Sequelize.BIGINT },
    PROPTS: { type: Sequelize.BIGINT },
    P_WADATIST: { type: Sequelize.STRING },
}, {
    timestamps: false,
    tableName: 'WORKORDERLINE',
})

module.exports = Workorder;