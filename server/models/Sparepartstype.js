const Sequelize = require('Sequelize');
const db = require('../config/database')

const Sparepartstype = db.define('SPAREPARTSTYPE', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    NAME: {
        type: Sequelize.STRING,
    },

}, {
    timestamps: false,
    tableName: 'SPAREPARTSTYPE',
})

module.exports = Sparepartstype;