const Sequelize = require('Sequelize');
const db = require('../config/database')

const Brand = db.define('BRANDS', {
    PK: {
        primaryKey: true,
        type: Sequelize.STRING,

    },
    NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },


}, {
    timestamps: false,
    tableName: 'BRANDS',
})

module.exports = Brand;