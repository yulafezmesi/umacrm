const Sequelize = require('Sequelize');
const db = require('../config/database')
const User = require('./User')
const Templates = require('./Templates')

const Activitiy = db.define('TICKETACTIVITIES', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true

    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    CREATEDBY: {
        type: Sequelize.INTEGER
    },
    MODIFIEDBY: {
        type: Sequelize.INTEGER
    },
    IS_DELETED: {
        type: Sequelize.BOOLEAN,
    },
    TICKET_PK: {
        type: Sequelize.INTEGER,
    },
    TYPE: {
        type: Sequelize.STRING,
    },
    TEXT: {
        type: Sequelize.STRING,
    },
    TEMP_PK: { type: Sequelize.INTEGER }


}, {
    timestamps: true,
    tableName: 'TICKETACTIVITIES',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS',
})

Activitiy.belongsTo(User, { as: "CreatedUser", foreignKey: 'CREATEDBY' })
Activitiy.belongsTo(User, { as: "ModifiedUser", foreignKey: 'MODIFIEDBY' })
Activitiy.belongsTo(Templates, { as: "Template", foreignKey: 'TEMP_PK' })
module.exports = Activitiy;