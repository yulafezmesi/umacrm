const Sequelize = require('Sequelize');
const db = require('../config/sapmasterdb.js')
const Workorderline = require('./Workorderline')

const Workorder = db.define('WORKORDER', {
    HJMPTS: { type: Sequelize.BIGINT },
    CREATEDTS: { type: Sequelize.DATE },
    MODIFIEDTS: { type: Sequelize.DATE },
    TYPEPKSTRING: { type: Sequelize.BIGINT },
    PK: { type: Sequelize.BIGINT, primaryKey: true },
    SEALED: { type: Sequelize.INTEGER },
    P_WORKORDERPRICE: { type: Sequelize.BIGINT },
    P_TRANSPORTATION: { type: Sequelize.BIGINT },
    P_BUSINESSFILETYPE: { type: Sequelize.BIGINT },
    P_SERIALNUMBER: { type: Sequelize.STRING },
    P_CHASSISNUMBER: { type: Sequelize.STRING },
    P_FILEDATE: { type: Sequelize.DATE },
    P_CURRENCY: { type: Sequelize.BIGINT },
    P_CUSTOMERCODE: { type: Sequelize.STRING },
    P_CUSTOMERNAMESURNAME: { type: Sequelize.STRING },
    P_CUSTOMERPHONE: { type: Sequelize.STRING },
    P_PRODUCTCODE: { type: Sequelize.STRING },
    P_PRODUCTDESCRIPTION: { type: Sequelize.STRING },
    P_PRODUCTBRAND: { type: Sequelize.STRING },
    P_PRODUCTMODEL: { type: Sequelize.STRING },
    P_APPROVALSTATUS: { type: Sequelize.BIGINT },
    P_WARRANTYSTATUS: { type: Sequelize.BIGINT },
    P_FITTER: { type: Sequelize.STRING },
    P_WARRANTYSTARTER: { type: Sequelize.STRING },
    P_CREATOR: { type: Sequelize.STRING },
    P_CREATORCODE: { type: Sequelize.STRING },
    P_SAPORDERNO: { type: Sequelize.STRING },
    P_SAPSASNO: { type: Sequelize.STRING },
    P_SENDINGSTATUS: { type: Sequelize.BIGINT },
    P_WORKORDERDESCRIPTION: { type: Sequelize.STRING },
    P_MAINTENANCE: { type: Sequelize.STRING },
    P_DEALER: { type: Sequelize.BIGINT },
    P_APPOINTMENTDATE: { type: Sequelize.DATE },
    P_CANCELLATIONDESC: { type: Sequelize.STRING },
    P_ISDELETED: { type: Sequelize.INTEGER },
    P_CODE: { type: Sequelize.STRING },
    P_REFERABLE: { type: Sequelize.INTEGER },
    P_ISOLD: { type: Sequelize.INTEGER },
    P_USER: { type: Sequelize.BIGINT },
    ACLTS: { type: Sequelize.BIGINT },
    PROPTS: { type: Sequelize.BIGINT },
    P_CUSTOMER: { type: Sequelize.BIGINT },
}, {
    timestamps: false,
    tableName: 'WORKORDER',
})

Workorder.hasMany(Workorderline, { as: "Workorderline", foreignKey: "P_WORKORDER" })

module.exports = Workorder;