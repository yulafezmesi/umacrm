const Sequelize = require('Sequelize');
const db = require('../config/database')

const Role = db.define('ROLES', {
    PK: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,

    },
    NAME: {
        type: Sequelize.STRING
    }
}, {

    timestamps: false,
    tableName: 'ROLES',
})

module.exports = Role;