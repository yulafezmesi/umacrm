const Sequelize = require('Sequelize');
const db = require('../config/database')

const CloseReason = db.define('TICKETCLOSEREASONS', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,

    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    CREATEDBY: {
        type: Sequelize.INTEGER
    },
    MODIFIEDBY: {
        type: Sequelize.INTEGER
    },
    NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },


}, {
    timestamps: true,
    tableName: 'TICKETCLOSEREASONS',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS',
})

module.exports = CloseReason;