const Sequelize = require('Sequelize');
const db = require('../config/database')

const BrandCategory = db.define('BRANDCATEGORIES', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    CATEGORY_PK: {
        type: Sequelize.INTEGER,
    },
    BRAND_PK: {
        type: Sequelize.INTEGER
    },
    NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },


}, {
    timestamps: false,
    tableName: 'BRANDCATEGORIES',
})

module.exports = BrandCategory;