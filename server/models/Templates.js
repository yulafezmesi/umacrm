const Sequelize = require('Sequelize');
const db = require('../config/database')

const Template = db.define('MAILTEMPLATES', {
    PK: {
        primaryKey: true,
        type: Sequelize.INTEGER,

    },
    NAME: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    TEMPLATE: {
        type: Sequelize.STRING,
        allowNull: false
    }

}, {
    timestamps: false,
    tableName: 'MAILTEMPLATES',
})

module.exports = Template;