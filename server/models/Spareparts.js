const Sequelize = require('Sequelize');
const db = require('../config/database')

const Spareparts = db.define('SPAREPARTS', {
    PK: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,

    },
    CREATEDTS: {
        type: Sequelize.DATE
    },
    MODIFIEDTS: {
        type: Sequelize.DATE
    },
    CREATEDBY: {
        type: Sequelize.INTEGER
    },
    MODIFIEDBY: {
        type: Sequelize.INTEGER
    },
    TICKET_PK: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Matnr: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Ailei: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Mrkid: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Anakg: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Altkg: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    Maktx: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Altkgt: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Labstks: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Labstgs: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    PerakendeFiyat: {
        type: Sequelize.STRING
    },
}, {
    timestamps: true,
    tableName: 'SPAREPARTS',
    createdAt: 'CREATEDTS',
    updatedAt: 'MODIFIEDTS',
})

module.exports = Spareparts;