const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const corsOptions = {
    exposedHeaders: 'auth-token',
    credentials: true,
    origin: true
};
const cors = require('cors');
const bodyParser = require('body-parser')
const indexRouter = require('./routes/index');
const customersRouter = require('./routes/api/customers');
const requirements = require('./routes/api/requirements');
const tickets = require('./routes/api/tickets');
const spareparts = require('./routes/api/spareparts');
const activities = require('./routes/api/activities');
const musteriBakiye = require('./routes/api/musteribakiye');
const login = require('./routes/api/login');
const auth = require('./routes/api/auth');
const sendmail = require('./routes/api/sendmail');
const invoice = require('./routes/api/invoice');
const workorder = require('./routes/api/workorders');
const dealers = require('./routes/api/dealers');
const ceksenet = require('./routes/api/ceksenet');
const memorandum = require('./routes/api/memorandum');
const reports = require('./routes/api/reports');
const orders = require('./routes/api/orderreport');
require('dotenv').config();
const db = require('./config/database')
db.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.json())
app.use(logger('dev'));
app.use(cors(corsOptions))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/api/customers', customersRouter);
app.use('/api/musteribakiye', musteriBakiye);
app.use('/api/requirements', requirements);
app.use('/api/login', login);
app.use('/api/tickets', tickets);
app.use('/api/spareparts', spareparts);
app.use('/api/activities', activities);
app.use('/api/auth', auth);
app.use('/api/sendmail', sendmail);
app.use('/api/invoice', invoice);
app.use('/api/workorders', workorder);
app.use('/api/dealers', dealers);
app.use('/api/ceksenet', ceksenet);
app.use('/api/memorandum', memorandum);
app.use('/api/reports', reports);
app.use('/api/orderreport', orders);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});




module.exports = app;