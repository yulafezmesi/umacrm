const path = require('path');
module.exports = {
    // devServer: {
    //     disableHostCheck: true,
    //     host: '0.0.0.0',
    //     port: 4000
    // },
    lintOnSave: true,
    outputDir: path.resolve(__dirname, process.env.DIST_FOLDER),
    configureWebpack: {
        optimization: {
            splitChunks: false
        }
    },
    filenameHashing: false,
    pages: {
        main: {
            entry: 'src/main.js',
            filename: 'index.html'
        }
    },
    "transpileDependencies": [
        "vuetify"
    ]
}