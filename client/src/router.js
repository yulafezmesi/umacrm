import VueRouter from "vue-router"
import Vue from 'vue'
import Login from "./components/Login";
import Customers from "./components/Customer/Customers";
import Customer from "./components/Customer/Customer";
import CustomerDetail from "./components/Customer/CustomerDetail";
import AddCustomer from "./components/Customer/AddCustomer";
import Tickets from "./components/Ticket/Tickets";
import NewTicket from "./components/Ticket/NewTicket";
import EditTicket from "./components/Ticket/EditTicket";
import AuthGuard from './auth-guard'


Vue.use(VueRouter);
export const router = new VueRouter({
    routes: [{
            path: '/giris',
            component: Login,
        }, {
            path: '/musteriler',
            component: Customers,
            beforeEnter: AuthGuard
        },
        {
            path: '/musteriler/yeni',
            component: AddCustomer,
            beforeEnter: AuthGuard
        },
        {
            path: '/musteriler/:id',
            component: Customer,
            beforeEnter: AuthGuard,
            props: true,
            children: [{
                    // UserProfile will be rendered inside User's <router-view>
                    // when /user/:id/profile is matched
                    path: 'detay',
                    component: CustomerDetail,
                    props: true
                },
                // {
                //     // UserPosts will be rendered inside User's <router-view>
                //     // when /user/:id/posts is matched
                //     path: 'posts',
                //     component: UserPosts
                // }
            ]
        },
        {
            path: '/talepler',
            component: Tickets,
            beforeEnter: AuthGuard
        },
        {
            path: '/talepler/yeni',
            component: NewTicket,
            beforeEnter: AuthGuard
        },
        {
            path: '/talepler/:id/duzenle',
            component: EditTicket,
            beforeEnter: AuthGuard
        },
        {
            path: '/',
            beforeEnter: AuthGuard,
        }
    ],
    mode: "history"
})