import httpRequest from 'axios'
const axios = httpRequest.create({
    baseURL: process.env.VUE_APP_DATABASE_URL,
});


/*
 * The interceptor here ensures that we check for the token in local storage every time an axios request is made
 */
axios.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('auth-token')

        if (token) {
            config.headers['auth-token'] = token;
        }

        return config
    },

    (error) => {
        return Promise.reject(error)
    }
)

export default axios