/* eslint-disable no-unused-vars */

import Vue from "vue"
import Vuex from "vuex"
import axios from './http-common'

/* eslint-disable no-console */
/* eslint-enable no-console */
// const instance =  axios.create({
//     baseURL: 'https://some-domain.com/api/',
//     timeout: 1000,
//     headers: { 'X-Custom-Header': 'foobar' }
// });
Vue.use(Vuex)

const getDefaultState = () => {
    return {
        invoices: [],
        workorders: [],
        models: [],
        spareparts: [],
        updatedSpareparts: [],
        brandCategories: [],
        dealers: [],
        subCategories: [],
        requirements: [],
        customers: [],
        tickets: [],
        ticketCount: null,
        customerCount: null,
        ticket: {},
        customer: {},
        customerPK: null,
        isEmited: false,
        latestActivity: {},
        addedCustomer: {},
        userProperties: {},
        authenticated: false,
        headers: {
            'auth-token': localStorage.getItem('auth-token')
        },
        loading: true,
    }
}

const store = new Vuex.Store({
    state: getDefaultState(),
    mutations: {
        resetState(state) {
            state.isAuthenticated = false;
            localStorage.clear();

        },
        setInvoices(state, payload) {
            state.invoices = payload
        },
        setWorkorders(state, payload) {
            state.workorders = payload
        },
        setSpareparts(state, payload) {
            state.spareparts = payload
        },
        setUpdatedSpareparts(state, payload) {
            state.updatedSpareparts = payload
        },
        setModels(state, payload) {
            state.models = payload
        },
        setSubCategories(state, payload) {
            state.subCategories = payload
        },
        setBrandCategories(state, payload) {
            state.brandCategories = payload
        },
        setRequirementValues(state, payload) {
            state.requirements = payload
        },
        setTicketValues(state, payload) {
            state.tickets = payload;
        },
        setTicketCount(state, paylaod) {
            state.ticketCount = paylaod
        },
        setTicketValue(state, payload) {
            state.ticket = payload;
        },
        setLatestActivity(state, payload) {
            state.latestActivity = payload
        },
        setHeader(state, payload) {
            state.headers['auth-token'] = payload
            state.authenticated = true
        },
        setUserProperties(state, payload) {
            state.userProperties = payload
        },
        setCustomerValues(state, payload) {
            state.customers = payload;
        },
        filteredTickets(state, payload) {
            state.tickets = payload
        },
        setDealerValues(state, payload) {
            state.dealers = payload;
        },
        setCustomerValue(state, payload) {
            state.customer = payload;
        },
        setCustomerCount(state, payload) {
            state.customerCount = payload
        },
        setAddedCustomerValue(state, payload) {
            state.addedCustomer = payload;
        },
        setAuth(state, payload) {
            state.authenticated = payload
        },
        setLoading(state, payload) {
            state.loading = payload
        },
        setEmitedCustomerPK(state, payload) {
            state.customerPK = payload
        },
        setEmit(state) {
            state.isEmited = true
        }
    },
    actions: {
        resetState({ commit }) {
            commit("resetState");
        },
        async userLogin({ commit }, payload) {
            const response = await axios.post("/auth/login", payload)
            if (response.status == 200) {
                commit("setAuth", true)
                localStorage.setItem('auth-token', response.headers['auth-token']) // store the token in localstorage
                axios.defaults.headers.common['auth-token'] = response.headers['auth-token'];
                commit("setHeader", response.headers['auth-token'])
            }
        },
        async getCurrentUser({ commit }) {
            const response = await axios.get(`/auth/getCurrentUser`);
            commit('setUserProperties', response.data);
        },
        async getCustomers({ commit }, payload) {
            // const response = await axios.get("/users");
            const response = await axios({
                    method: 'get',
                    url: '/customers',
                    params: {
                        limit: payload.limit,
                        offset: payload.offset
                    }
                },

            )
            commit('setCustomerValues', response.data);
        },
        async getCustomer({ commit }, id) {
            const response = await axios.get(`/customers/${id}`);
            commit('setCustomerValue', response.data);
        },
        async getCustomerTickets({ commit }, id) {
            const response = await axios.get(`/customers/${id}`);
            commit('filteredTickets', response.data.Tickets);
            /* eslint-disable no-console */
            console.log(response.data.Tickets)
                /* eslint-enable no-console */
        },
        async getCustomerByName({ commit }, name) {
            const response = await axios.get(`/customers/ara/${name}`)
            if (response.data) {

                commit('setCustomerValues', response.data);
            }
        },
        async getDealers({ commit }, name) {
            const response = await axios.get(`/dealers`, {
                params: {
                    M_ADI: name,
                }
            });
            if (response.data) {

                commit('setDealerValues', response.data);
            }
        },
        async addCustomer({ commit }, customer) {
            let { P_NAME } = customer;
            if (P_NAME) {
                commit("setAddedCustomerValue", customer)
                return await axios.post(`/customers/add/`, customer)
            } else {
                throw new Error("Bilinmeyen bir hata oluştu, lütfen tekrar giriş yapınız...");
            }

        },
        async getCustomerCount({ commit }) {
            const response = await axios.get('/customers/count')
            commit('setCustomerCount', response.data.count)

        },
        async getTickets({ commit }, payload) {
            /* eslint-disable no-console */
            console.log(payload)
                /* eslint-enable no-console */
            const response = await axios.get(`/tickets`, {
                params: {
                    limit: payload.limit,
                    offset: payload.offset,
                    query: payload.query
                }

            });


            commit('setTicketValues', response.data);
        },
        async getMyTickets({ commit }, payload) {
            /* eslint-disable no-console */
            console.log(payload)
                /* eslint-enable no-console */
            const response = await axios.get(`/tickets/my`, {
                // Backend tarafında user parametresi gönderiliyor
                params: {
                    limit: payload.limit,
                    offset: payload.offset
                }
            });
            commit('setTicketValues', response.data);
        },
        async getTicket({ commit }, pk) {
            const response = await axios.get(`/tickets/${pk}`);
            commit('setTicketValue', response.data)
            commit('setLoading', false)

        },
        async getTicketCount({ commit }, pk) {
            const response = await axios.get('/tickets/count')
            commit('setTicketCount', response.data.count)

        },
        async addTicket({ commit }, payload) {
            return axios.post(`/tickets/add/`, payload)

        },
        async updateTicket({ commit }, payload) {
            return await axios.put(`/tickets/update/`, payload)

        },
        async updateCustomer({ commit }, payload) {
            return await axios.put(`/customers/update/`, payload)

        },
        async getRequirements({ commit }) {
            let requirements = [];
            let CloseReason = await axios({
                method: 'get',
                url: '/requirements/CloseReason',
            })
            let Priority = await axios({
                method: 'get',
                url: '/requirements/Priority',
            })
            let ReasonCategory = await axios({
                method: 'get',
                url: '/requirements/ReasonCategory',
            })
            let ReasonSubCategory = await axios({
                method: 'get',
                url: '/requirements/ReasonSubCategory',
            })
            let RootReason = await axios({
                method: 'get',
                url: '/requirements/RootReason',
            })
            let SatisfiedState = await axios({
                method: 'get',
                url: '/requirements/SatisfiedState',
            })
            let Source = await axios({
                method: 'get',
                url: '/requirements/Source',
            })
            let State = await axios({
                method: 'get',
                url: '/requirements/State',
            })
            let Type = await axios({
                method: 'get',
                url: '/requirements/Type',
            })
            let Brand = await axios({
                method: 'get',
                url: '/requirements/Brand',
            })
            let Templates = await axios({
                method: 'get',
                url: '/requirements/mailtemplates',
            })
            let Sparepartstype = await axios({
                method: 'get',
                url: '/requirements/sparepartstype',
            })
            let Responsible = await axios({
                method: 'get',
                url: '/requirements/Responsible',
            })
            requirements['CloseReason'] = CloseReason.data
            requirements['Priority'] = Priority.data
            requirements['ReasonCategory'] = ReasonCategory.data
            requirements['ReasonSubCategory'] = ReasonSubCategory.data
            requirements['RootReason'] = RootReason.data
            requirements['SatisfiedState'] = SatisfiedState.data
            requirements['Source'] = Source.data
            requirements['State'] = State.data
            requirements['Type'] = Type.data
            requirements['Brand'] = Brand.data
            requirements['Templates'] = Templates.data
            requirements['Sparepartstype'] = Sparepartstype.data
            requirements['Responsible'] = Responsible.data
            commit("setRequirementValues", requirements)
        },
        async getModels({ commit }, payload) {
            const response = await axios.get(`/requirements/model`, {
                params: {
                    brand: payload.brand,
                    name: payload.name
                }
            });

            commit('setModels', response.data.d.results);
        },
        async getBrandCategories({ commit }, payload) {
            const response = await axios.get(`/requirements/getbrandcategories`, {
                params: {
                    brand: payload.brand
                }
            });
            commit('setBrandCategories', response.data);
        },
        async getSubCategories({ commit }, payload) {
            const response = await axios.get(`/requirements/getsubcategories`, {
                params: {
                    brand: payload.brand,
                    category: payload.category
                }
            });

            commit('setSubCategories', response.data);
        },
        async getUpdatedSpareparts({ commit }, pk) {
            const response = await axios.get(`/tickets/${pk}`);
            commit('setUpdatedSpareparts', response.data['Spareparts'])
        },
        async getSpareparts({ commit }, payload) {
            const response = await axios.get(`/spareparts`, {
                params: {
                    subCategory: payload.subCategory
                }
            });
            commit('setSpareparts', response.data);
        },
        async getWorkorders({ commit }, payload) {
            const response = await axios.get(`/workorders`, {
                params: {
                    chassis: payload
                }
            });

            commit('setWorkorders', response.data);
        },
        async getInvoices({ commit }, payload) {
            const response = await axios.get(`/invoice`, {
                params: {
                    chassis: payload
                }
            });
            commit('setInvoices', response.data);
        },
        async addSpareparts({ commit }, payload) {
            return await axios.post(`/spareparts/add`, payload);
        },
        async removeSpareparts({ commit }, payload) {

            return await axios.delete(`/spareparts/delete`, {
                params: {
                    PK: payload,
                }
            })
        },
        async addActivity({ commit }, payload) {
            return await axios.post(`/activities/add`, payload)

        },
        async updateActivity({ commit }, payload) {
            return await axios.put(`/activities/update`, payload)
        },
        async deleteActivity({ commit }, payload) {
            return await axios.delete(`/activities/delete`, {
                params: {
                    PK: payload,
                }
            })
        },
        async getLatestActivity({ commit }, payload) {
            const response = await axios.get(`/activities/latest`, {
                params: {
                    PK: payload,
                }
            })
            commit('setLatestActivity', response.data);

        },
        async sendMail({ commit }, payload) {
            return axios.post(`/sendmail`, payload)

        },
        emitedCustomerPK({ commit }, payload) {
            commit('setEmitedCustomerPK', payload);
            commit('setEmit');
        },
        destroyEmit({ state }) {
            state.isEmited = false
        }
    },
    getters: {
        userHeader(state) {
            return state.headers
        },
        user(state) {
            return state.userProperties
        },
        isAuthenticated(state) {
            if (state.authenticated || localStorage.getItem('auth-token')) {
                return true
            } else {
                return false
            }
        },
        customers(state) {
            return state.customers;
        },
        tickets(state) {
            return state.tickets;
        },
        ticketCount(state) {
            return state.ticketCount
        },
        customerCount(state) {
            return state.customerCount
        },
        ticket(state) {
            return state.ticket
        },
        customer(state) {
            return state.customer
        },
        addedCustomer(state) {
            return state.addedCustomer
        },
        requirements(state) {
            return state.requirements
        },
        isLoading(state) {
            return state.loading
        },
        models(state) {
            return state.models
        },
        brandCategories(state) {
            return state.brandCategories
        },
        subCategories(state) {
            return state.subCategories
        },
        spareparts(state) {
            return state.spareparts
        },
        updatedSpareparts(state) {
            return state.updatedSpareparts
        },
        latestActivity(state) {
            return state.latestActivity
        },
        invoices(state) {
            return state.invoices
        },
        workorders(state) {
            return state.workorders
        },
        customerPK(state) {
            return state.customerPK
        },
        dealers(state) {
            return state.dealers
        },

    },
})

export default store
/* eslint-enable no-unused-vars */