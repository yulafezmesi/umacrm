import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from "./store"
import { router } from './router'
import JsonExcel from 'vue-json-excel'

Vue.component('downloadExcel', JsonExcel)
Vue.config.productionTip = false
    // import axios from 'axios';
    // const axiosMap = axios.create({
    //     baseURL: process.env.VUE_APP_DATABASE_URL,
    // });
    /* eslint-disable no-console */

/* eslint-enable no-console */

//Use the window object to make it available globally.
// window.axios = axiosMap;
new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app')